(defproject clojure-native-image "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  ;; clojure version "1.10.2-alpha1" includes fixes for some graalvm specific issues
  ;; see https://clojure.org/community/devchangelog#_release_1_10_2
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.clojure/core.async "1.3.618"]
                 ;; [amazonica "0.3.156"]
                 ;; [org.clojure/clojure "1.10.0"]
                 [ring/ring-core "1.8.1"]
                 [lynxeyes/dotenv "1.1.0"]
                 [ring/ring-jetty-adapter "1.8.1"]
                 [nrepl "0.8.3"]
                 [hiccup "1.0.5"]]
  ;; add the main namespace
  :main clojure-native-image.core

  ;; add AOT compilation
  :profiles {:uberjar {:aot :all}}
  )

