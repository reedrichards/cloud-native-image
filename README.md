# clojure-native-image

A Clojure library designed to ... well, that part is up to you.

- https://gitlab.com/reedrichards/cloud-native-image

try it out at [https://n83pj53hb7.us-east-1.awsapprunner.com/](https://n83pj53hb7.us-east-1.awsapprunner.com/)

## Quickstart

if you have [just](https://github.com/casey/just) and [docker](https://docs.docker.com/get-docker/) installed, you can
start the project with `just run`. Otherwise run

```shell
$ docker run -p 3000:3000 cloud-native-image:latest
```

## Development

https://stackoverflow.com/questions/2706044/how-do-i-stop-jetty-server-in-clojure

```clojure
(defonce server (run-jetty #'my-app {:port 8080 :join? false}))

<!-- This prevents locking up the REPL. It also allows me to recompile this file without worrying that my server will get redefined. It also lets you interact at the REPL like so: -->

user=> (.stop server)

and

user=> (.start server)
```

## Setup and Configuration

create an iam user with the following permissions for terraform:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "ListImagesInRepository",
      "Effect": "Allow",
      "Action": ["ecr:ListImages"],
      "Resource": "arn:aws:ecs:us-east-1:150301572911:repository/cloud-native-image"
    },
    {
      "Sid": "GetAuthorizationToken",
      "Effect": "Allow",
      "Action": ["ecr:GetAuthorizationToken"],
      "Resource": "*"
    },
    {
      "Sid": "ManageRepositoryContents",
      "Effect": "Allow",
      "Action": ["ecr:*"],
      "Resource": "arn:aws:ecr:us-east-1:150301572911:repository/cloud-native-image"
    },
    {
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": [
        "apprunner:ListConnections",
        "apprunner:ListAutoScalingConfigurations",
        "apprunner:ListServices",
        "iam:*"
      ],
      "Resource": "*"
    },
    {
      "Sid": "VisualEditor1",
      "Effect": "Allow",
      "Action": "apprunner:*",
      "Resource": [
        "arn:aws:apprunner:us-east-1:150301572911:connection/*/*",
        "arn:aws:apprunner:us-east-1:150301572911:autoscalingconfiguration/*/*/*",
        "arn:aws:apprunner:us-east-1:150301572911:service/*/*"
      ]
    }
  ]
}
```

example terraform

```hcl
resource "aws_iam_user" "cloud_native_image" {
  name = "cloud_native_image"

  tags = {
    Project  = "cloud_native_image"
    Type  = "terraform"
  }
}

data "template_file" "cloud_native_image" {
  template = file("./policies/cloud_native_image.json")
}
resource "aws_iam_user_policy" "cloud_native_image" {
  name = "cloud_native_image"
  user = aws_iam_user.cloud_native_image.name

  policy = data.template_file.cloud_native_image.rendered
}

```

configure https://gitlab.com/reedrichards/cloud_native_image/-/settings/ci_cd
for with access key terraform user

create access key https://console.aws.amazon.com/iam/home#/users/cloud-native-image?section=security_credentials

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

push this repo to the new project on gitlab

```shell
git init --initial-branch=main
git remote add origin git@gitlab.com:reedrichards/cloud-native-image.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
