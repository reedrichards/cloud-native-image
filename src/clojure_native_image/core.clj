(ns clojure-native-image.core
  (:gen-class)
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.response :as response]
            [hiccup.core :as hiccup]
            [hiccup.page :as page]))

(def homepage 
  (hiccup/html
   [:head (page/include-css "https://cdn.jsdelivr.net/npm/water.css@2/out/water.css")]
   [:body
    [:p "This website is built using "
     [:a {:href "https://gitlab.com/reedrichards/cloud-native-image/"} "this gitlab repo"]
     " as a statically compiled native docker image and runs on aws apprunner"]]))

(defn handler [{uri :uri}]
  (case uri
    "/" (-> (response/response homepage))
    "/health" (-> (response/response "OK"))
    (response/not-found "Not Found")))

(defn -main []
    (jetty/run-jetty handler {:port 3000}))
