{
  description = "Flake with nixpkgs import example";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }: 
    flake-utils.lib.eachDefaultSystem (system: 
      let
        # Import nixpkgs and inherit the system
        pkgs = import nixpkgs { inherit system; };
      in {
        packages = {
          hello = pkgs.hello;
        };
        devShell = pkgs.mkShell {
            pkgs.hello
        }
      }
    );
}
