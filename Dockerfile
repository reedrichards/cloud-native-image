FROM ghcr.io/graalvm/graalvm-ce:21.2.0 as builder

RUN curl -O https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein && \
    chmod +x lein && \
    mv lein /usr/bin/lein && \
    lein upgrade

RUN gu install native-image

COPY project.clj project.clj
RUN lein deps
COPY . .
RUN lein do clean, uberjar &&  native-image --report-unsupported-elements-at-runtime \
		--initialize-at-build-time \
		--no-fallback \
		--no-server \
		--static \
		-jar ./target/clojure-native-image-0.1.0-SNAPSHOT-standalone.jar \
		-H:Name=./target/clojure-native-image-0.1.0-SNAPSHOT-standalone

FROM scratch
COPY --from=builder /target/clojure-native-image-0.1.0-SNAPSHOT-standalone /target/clojure-native-image-0.1.0-SNAPSHOT-standalone

EXPOSE 3000
# Run the hello binary.
ENTRYPOINT ["/target/clojure-native-image-0.1.0-SNAPSHOT-standalone"]
