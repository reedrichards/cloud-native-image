build:
	docker build . -t cloud-native-image

kill:
	docker ps | grep 3000 | awk '{ print $1 }' | xargs docker kill

run: build
	docker run -p 3000:3000 cloud-native-image:latest
